promise_fun = require("../promise_fun");

// we return a resolved promise but the return value is actually not wanted
// -> we "catch" this in the then and return a rejected promise 
// -> this rejected promise is then directly handled by catch, i.e. we skip the second then.
promise_fun
  .myPromiseFunc((shouldError = false), (isUnwantedRetValue = true))
  .then(retValue => {
    if (retValue) {
      // return value is unwanted
      //throw new Error("promise returned soemthing bad:", retValue);
      return Promise.reject(Error("this was rejected"));
    }
  })
  .then(() => console.log("this is not executed."))
  .catch(error => console.log(error));
