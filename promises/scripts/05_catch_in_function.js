promise_fun = require("../promise_fun");

const catchInFunction = () => {
  // this promise resolves because return value is not unwanted (second argument = false)
  return promise_fun
    .myPromiseFunc(false, true)
    .then(isUnwanted => {
      if (isUnwanted) {
        return Promise.reject(Error("catching this in the function"));
      }
    })
    .catch(err => console.log("caught the error in the function!"));
};

// the function chain is not broken by the error in catchInFunction because we handle 
// the error in the function -> the returned promise is in resolved state and we continue with then.
catchInFunction()
  .then(() => console.log("hello! the function chain continues!"))
  .catch(err => console.log("this is not executed"));
