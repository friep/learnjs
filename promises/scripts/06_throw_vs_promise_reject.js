promise_fun = require("../promise_fun");

const catchInFunction = () => {
  // this promise rejects because return value is unwanted and there is no error handling.
  // throw Error behaves like Promise.reject(Error(""))
  // advantage of promise.reject() -> we can also reject just with a string or other return data. 
  return promise_fun
    .myPromiseFunc(false, true)
    .then(isUnwanted => {
      if (isUnwanted) {
        throw Error("throwing behaves like Promise.reject")
      }
    })
};

// the function chain is broken by the thrown error in the function
catchInFunction()
  .then(() => console.log("this is not executed"))
  .catch(err => console.log(err));
