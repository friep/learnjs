promise_fun = require("../promise_fun");

// function to stringify an error. if "error" argument is a string, we just take the string
const stringifyErr = (error, beforeTxt = '') => {
    strErr = `${beforeTxt} ${error.stack ? error.stack: error}}`
    return strErr
}


const funWithErrorRejectOnly = () => {
  return myPromiseFunc(false, true).then(isUnwanted => {
    if (isUnwanted) {
      return Promise.reject("just rejecting with a string")
    }
  })
}

promise_fun
  .funWithErrorRejectOnly()
  .then(() => {
    console.log("not executed");
  })
  .catch(error => console.log("an error in the chain", stringifyErr(error)));
