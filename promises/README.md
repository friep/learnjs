# promises

## resources
- [Asynchronous Javascript module from Mozilla](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous)
- [introduction video](https://www.youtube.com/watch?time_continue=1&v=QO4NXhWo_NM&feature=emb_title)
- [very good blogpost / video](https://tylermcginnis.com/async-javascript-from-callbacks-to-promises-to-async-await/)

## terminology
- synchronous code: code that runs from top to bottom. If a function takes long to compute, this will *block* subsequent code until the function is completed.
- asynchronous code: allows a program to do more than one thing at a time. 


## what are promises?
Promise "is an object that represents an intermediate state of an operation" ([Mozilla](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Promises)). They are used to run code asynchronously. 

They can have three states:
- pending
- fulfilled
- rejected

## async/await