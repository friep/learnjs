const myPromiseFunc = (shouldError, retValue) => {
  // this function takes two parameters:
  // shouldError: if true, directly reject the promise -> returns a promise with state rejected. 
    // if false, the promise is resolved and returns the isUnwantedRetValue parameter.
  // retValue: true or false. This is supposed to be then used in subsequent .then() statements to
    // see how we can throw errors from then based on results from previous promises.
  return new Promise((resolve, reject) => {
    if (shouldError) {
      reject(Error("we got an error value!"));
    }

    resolve(retValue);
  });
};

module.exports = {
  myPromiseFunc,
};
