promise_fun = require("../promise_fun");
jest = require("jest");
describe("rejected promises", () => {
  it("should reject a rejected promise and should skip then statements", () => {
    expect.assertions(1);
    expect(
      promise_fun
        .myPromiseFunc((shouldError = true), (isUnwantedRetValue = false)) // this promise rejects
        .then(retValue => {
          // this is not executed because the promise is rejected
          console.log("this is not executed");
        })
        .then(() => console.log("this is not executed."))
    ).rejects.toEqual({
      error: "we got an error value!"
    });
  });
});
